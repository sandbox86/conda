#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 12:57:57 2021

@author: frigo
"""

import tensorflow as tf
from tensorflow.keras import initializers

initializer = initializers.RandomNormal(mean=0., stddev=1.)
values = initializer(shape=(4,4))
tf.print(values)