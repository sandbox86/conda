# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 12:11:51 2021

@author: frigo
"""

import tensorflow as tf
from tensorflow.keras import layers, Input
from tensorflow.keras.models import Sequential

x = tf.ones([2,2])
tf.print(x)

model = Sequential()
layer = layers.Dense(units=5, 
                     input_dim=2,
                     kernel_initializer='random_normal', 
                     bias_initializer='zeros')
dummy_input = Input(name='dummy_input', shape=[1,2], dtype=tf.dtypes.float32)
# tf.placeholder(tf.float32, [None, 2], name='dummy_input')
y = layer(dummy_input)
print(layer.trainable_weights)
