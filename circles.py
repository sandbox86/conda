from sklearn.datasets import make_circles
X, y = make_circles(n_samples=100, noise=0.1, random_state=1)
a, b = X.T
import numpy as np
colormap = np.array(['r', 'g'])
from matplotlib import pyplot as plt
plt.scatter(a, b, c=colormap[y])
plt.show()
